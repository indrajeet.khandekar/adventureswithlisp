`
(define s `a)
(define l `((b) c d))
(cons s (car l))
(cons s (cdr l))

(null? `spagetti)
(null? `())
(null? `(a b c))
(null? ())
(null? (quote ()))

(define (atom? x)
  (and (not (pair? x))
       (not (null? x))))

(atom? `spagetti)
(atom? `())
(atom? `(a b c))
(atom? ())
(atom? (quote ()))

(pair? `spagetti)
(pair? `())
(pair? `(a b c))
(pair? ())
(pair? (quote ()))


(eq? `a `b)

(eq? (cdr `(soured milk)) `milk)
(eq? (car (cdr `(soured milk))) `milk)

(define (lat? l)
  (cond
    ((null? l) #t)
    ((atom? (car l)) (lat? (cdr l)))
    (else #f)))

(lat? `(jack sprat could not eat chicken))
(lat? `((jack) sprat could not eat chicken))
(lat? `(jack (sprat could) not eat chicken))
(lat? ())

(define member?
  (lambda (x l)
    (cond
      ((null? l) #f)
      (else (or (eq? x (car l))
                (member? x (cdr l)))))))
(member? `tea `(eggs fried tea))
(member? `tea `(eggs fried))

(define rember
  (lambda (x l)
    (cond
      ((null? l) ())
      (else (cond
              ((eq? x (car l)) (rember x (cdr l)))
              (else (cons (car l) (rember x (cdr l)))))))))

(rember `mint `(mint lamb chops and mint jelly))


(define (firsts l)
  (cond
    ((null? l) ())
    (else (cons (car (car l)) (firsts (cdr l))))))

(firsts `(((five plums) four)
          (eleven green oranges)
          ((no) more)))


(firsts (cons `a (cons `c (cons `e ()))))

(define (insertR new old lat)
  (cond
    ((null? lat) ())
    (else (cond
            ((eq? old (car lat))
             (cons old (cons new (insertR new old (cdr lat)))))
            (else (cons (car lat) (insertR new old (cdr lat))))))))
(define (insertL new old lat)
  (cond
    ((null? lat) ())
    (else (cond
            ((eq? old (car lat))
             (cons new lat))
            (else (cons (car lat) (insertL new old (cdr lat))))))))
(define (subset new old lat)
  (cond
    ((null? lat) ())
    (else (cond
            ((eq? old (car lat))
             (cons new (subset new old (cdr lat))))
            (else (cons (car lat) (subset new old (cdr lat))))))))

(define (subset2 new o1 o2 lat)
  (cond
    ((null? lat) ())
    (else (cond
            ((or (eq? o1 (car lat)) (eq? o2 (car lat)))
             (cons new (subset2 new o1 o2 (cdr lat))))
            (else (cons (car lat) (subset2 new o1 o2 (cdr lat))))))))
(insertR `topping `fudge `(ice fudge cream with fudge for desert)) (insertL `topping `fudge `(ice cream with fudge for desert))
(subset `topping `fudge `(ice cream with fudge for desert))
(subset2 `topping `fudge `desert `(ice cream with fudge for desert))


(define (add1 x)
  (+ x 1))

(define (sub1 x)
  (if (zero? x)
    x
    (- x 1)))

(define (addtup tup)
  (if (null? tup)
    0
    (+ (car tup) (addtup (cdr tup)))))

(addtup (list 4 3 2 1 5))

(define (x n m)
  (if (zero? m) 0
    (+ n (x n (sub1 m)))))

(define (tup+ t1 t2)
  (if (or (null? t1) (null? t2))
    ()
    (cons (+ (car t1) (car t2))
          (tup+ (cdr t1) (cdr t2)))))
(tup+ `(1 2 3 4) `(1 2 3 4))
(define (tup+ t1 t2)
  (cond
    ((null? t1) t2)
    ((null? t2) t1)
    (else (cons (+ (car t1) (car t2))
                (tup+ (cdr t1) (cdr t2))))))
(tup+ `(1 2) `(1 2 3 4))

(define (> x y)
  (cond
    ((zero? x) #f)
    ((zero? y) #t)
    (else (> (sub1 x) (sub1 y)))))
(> 12 133)
(> 120 11)
(> 4 4)
(define (< x y)
  (cond
    ((zero? y) #f)
    ((zero? x) #t)
    (else (< (sub1 x) (sub1 y)))))
(< 12 133)
(< 120 11)
(< 4 4)

(define (= x y)
  (cond
    ((> x y) #f)
    ((< x y) #f)
    (else #t)))
(= 4 5)
(= 5 5)

(define (^ x y)
  (cond
    ((zero? y) 1)
    (else (* x (^ x (sub1 y))))))
(^ 2 3)
(^ 5 3)

(define (quotient n m)
  (cond ((< n m) 0)
        (else (add1 (quotient (- n m) m)))))

(quotient 15 4)


(define length 
  (lambda (l)
    (cond 
      ((null? l) 0)
      (else (add1 (length (cdr l)))))))

(length `(1 2 3 4 5))

(define pick
  (lambda (n l)
    (cond 
      ((null? l) #f)
      ((zero? (sub1 n)) (car l))
      (else (pick (sub1 n) (cdr l))))))

(pick 0 `(1 2 3 4 5))
(pick 3 `(1 2 3 4 5))
(pick 9 `(1 2 3 4 5))
(pick -1 `(1 2 3 4 5))

(define rempick
  (lambda (n l)
    (cond
    ((zero? (sub1 n)) (cdr l))
    (else (cons (car l) (rempick (sub1 n) (cdr l)))))))

(rempick 3 `(1 2 3 4 5))
(rempick 3 `(hotdogs with hot mustard))

(define no-nums
  (lambda (l)
    (cond
      ((null? l) `())
      (else (cond ((number? (car l)) (no-nums (cdr l)))
              (else (cons (car l) (no-nums (cdr l)))))))))

(no-nums `(1 2 3 4))
(no-nums `(1 hotdogs 2 with 4 6 hot mustard))

(define all-nums
  (lambda (l)
    (cond
      ((null? l) `())
      (else (cond ((number? (car l)) 
                   (cons (car l) (all-nums (cdr l))))
              (else (all-nums (cdr l))))))))

(all-nums `(1 2 3 4))
(all-nums `(1 hotdogs 2 with 4 6 hot mustard))

(define eqan?
  (lambda (a1 a2)
    (cond 
      ((and (number? a1) (number? a2)) (= a1 a2))
      ((or (number? a1) (number? a2)) #f)
      (else (eq? a1 a2)))))

(define occur
  (lambda (a l)
    (cond 
      ((null? l) 0)
      (else (cond
              ((eq? (car l) a) (add1 (occur a (cdr l))))
              (else (occur a (cdr l))))))))
(occur `hotdogs`(1 hotdogs 1 with 4 1 hotdogs mustard))
(define (one? x)
  (= x 1))
(one? 1)
(one? 0)
(one? a)

(define rempick-m
  (lambda (n l)
    (cond
    ((one? n) (cdr l))
    (else (cons (car l) (rempick (sub1 n) (cdr l)))))))
(rempick-m 3 `(1 2 3 4 5))
(rempick-m 3 `(hotdogs with hot mustard))

(define rember*
  (lambda (a l)
    (cond 
      ((null? l) `())
      ((atom? (car l)) 
       (cond 
         ((eq? a (car l)) (rember* a (cdr l)))
         (else
           (cons (car l) (rember* a (cdr l))))))
      (else (cons (rember* a (car l))
                  (rember* a (cdr l)))))))
      
(rember* `sauce `(((tomato sauce))
                 ((bean) sauce)
                 (and ((flying)) sauce)))

(define lat
  (lambda (l)
    (cond 
      ((null? l) 0)
      ((not (atom? (car l))) #f)
      (else (add1 (lat (cdr l)))))))

(define insertR
  (lambda (new old l)
    (cond
      ((null? l) `())
      ((atom? (car l))
       (cond 
         ((eq? old (car l)) 
          (cons old 
                (cons new 
                      (insertR new old (cdr l)))))
         (else (cons (car l)
                     (insertR new old (cdr l))))))
      (else (cons (insertR new old (car l))
                  (insertR new old (cdr l)))))))

(insertR `roast `chuck `((how much (wood))
                         could
                         ((a (wood) chuck))
                         (((chuck)))
                         (i f (a) ((wood chuck)))
                         could chuck wood))

(define occur*
  (lambda (x l)
    (cond 
      ((null? l) 0)
      ((atom? (car l))
       (cond 
         ((eq? x (car l))
          (add1 (occur* x (cdr l))))
         (else (occur* x (cdr l)))))
      (else (+ (occur* x (car l))
               (occur* x (cdr l)))))))

              
(occur* `banana `((banana)
                 (split ((((banana ice)))
                         (crea m (banana))
                         sherbet))
                 (banana)
                 (bread)
                 (banana brandy)))

(define subst*
  (lambda (x y l)
    (cond 
      ((null? l) `())
      ((atom? (car l))
       (cond 
         ((eq? x (car l))
          (cons y (subst* x y (cdr l))))
         (else (cons (car l) (subst* x y (cdr l))))))
       (else (cons (subst* x y(car l))
                   (subst* x y(cdr l)))))))

              
(subst* `banana `orange `((banana)
                          (split ((((banana ice)))
                                  (crea m (banana))
                                  sherbet))
                          (banana)
                          (bread)
                          (banana brandy)))

(define insertR
  (lambda (x y l)
    (cond 
      ((null? l) `())
      ((atom? (car l))
       (cond 
         ((eq? x (car l))
          (cons x (cons y (insertR x y (cdr l)))))
         (else (cons (car l) (insertR x y (cdr l))))))
       (else (cons (insertR x y (car l))
                   (insertR x y (cdr l)))))))

              
(insertR `chuck `pecker ` ((how much (wood))
                           could
                           ((a (wood) chuck))
                           (((chuck)))
                           (if (a) ((wood chuck)))
                           cou ld chuck wood)
        )


(define member*
  (lambda (x l)
    (cond 
      ((null? l) #f)
      ((atom? (car l))
       (cond 
         ((eq? x (car l))
          #t)
         (else (member* x (cdr l)))))
       (else (or (member* x (car l))
                   (member* x (cdr l)))))))

(member* `chips `((potato) (chips ((with) fish) (chips))))

(define leftmost
  (lambda (l)
    (cond 
      ((atom? (car l)) (car l))
      (else (leftmost (car l))))))
       


(define eqlist?
  (lambda (l1 l2)
    (cond
        ((and (null? l1) (null? l2)) #t)
        ((null? l1) #f)
        ((null? l2) #f)
        ((and (null? l1) (atom? (car l2))) #f)
        ((and (atom? (car l1)) (null? l2)) #f)
        ((and (atom? (car l1)) (atom? (car l2)))
         (and (eqan? (car l1) (car l2)) (eqlist? (cdr l1) (cdr l2))))
        ((atom? (car l1)) #f)
        ((atom? (car l2)) #f)
        (else
            (and (eqlist? (car l1) (car l2))
                 (eqlist? (cdr l1) (cdr l2)))))))

(define equal?
  (lambda (l1 l2)
    (cond
      ((and (atom? l1) (atom? l2)) (eqan? l1 l2))
      ((or  (atom? l1) (atom? l2)) #f)
      (else (eqlist? l1 l2))))

  (define eqlist?
    (lambda (l1 l2)
      (cond
        ((and (null? l1) (null? l2)) #t)
        ((or (null? l1) (null? l2)) #f)
        (else
          (and (equal? (car l1) (car l2))
               (eqlist? (cdr l1) (cdr l2)))))))
         
