;; Chapter 5
;; Handling text is not a computer’s strength. It is a necessary evil best kept to a minimum.
;;
(defparameter *nodes* '((living-room (you are in the living-room.
                                  a wizard is snoring loudly on the couch.))
                (garden (you are in a beautiful garden.
                             there is a well in fornt of you.))
                (attic (you are in the attic.
                            there is a giant welding torch in the corner.
                            ))))

(defun describe_location (location nodes)
  (cadr (assoc location nodes)))

(defparameter *edges* '((living-room (garden west door )
                                     (attic upstairs ladder))
                        (garden (living-room east door))
                        (attic (living-room downstairs ladder))))

(defun describe_path (edge)
  `(there is a ,(caddr edge) going,(cadr edge) from here.)) ;; Quasiquoting

(defun describe_paths (location edges)
  (apply #'append (mapcar #'describe_path (cdr (assoc location edges)))))

(defparameter *objects* '(bucket chain whiskey frog))

(defparameter *objects_location* '((bucket living-room)
                                   (whiskey living-room)
                                   (chain garden)
                                   (frog garden)))

(defun object_at (loc objs obj-locs)
  (labels ((at-loc-p (obj)
             (eq (cadr (assoc obj obj-locs)) loc)))
    (remove-if-not #'at-loc-p objs)))

(defun describe_objects (loc objs obj-locs)
  (labels ((describe_obj (obj)
             `(you see ,obj on the floor.)))
    (apply #'append (mapcar #'describe_obj (object_at loc objs obj-locs)))))

(defparameter *location* 'living-room)

(defun look ()
  (append (describe_location *location* *nodes*)
          (describe_paths *location* *edges* )
          (describe_objects *location* *objects* *objects_location*)))

(defun walk (direction)
  (let ((next (find direction 
                    (cdr (assoc *location* *edges*))
                    :key #'cadr))) ;; chapeter 7 for :key
   (if next 
     (progn (setf *location* (car next))
            (look))
     '(you cannot go that way.))))

(defun pickup (object)
  (cond ((member object 
                 (object_at *location* *objects* *objects_location*))
         (push (list object 'body) *objects_location*)
         `(you are now carrying the ,object))
        (t '(you cannot get that.))))

(defun inventory ()
  (cons 'items- (object_at 'body *objects* *objects_location*)))

(defun game-repl ()
  (let ((cmd (game-read)))
    (unless (or (eq (car cmd) 'quit) (eq (car cmd)'q) )
      (game-print (game-eval cmd))
      (game-repl))))

(defun game-read ()
  (let ((cmd (read-from-string
               (concatenate 'string "(" (read-line) ")"))))
    (flet ((quote-it (x)
             (list 'quote x)))
      (cons (car cmd) (mapcar #'quote-it (cdr cmd))))))

(defparameter *allowed-commands* '(look walk pickup inventory))

(defun game-eval (sexp)
  (if (member (car sexp) *allowed-commands*)
    (eval sexp)
    '(i dont knwo this command )))

;; some error in this code
(defun tweak-text (lst caps lit)
  (when lst
    (let ((item (car lst))
          (rest (cdr lst)))
      (cond ((eq item #\Space ) (cons item (tweak-text rest caps lit)))
            ((member item '(#\! #\? #\.)) (cons item (tweak-text rest t lit)))
            ((eq item #\") (tweak-text rest caps (not lit)))
            (lit (cons item (tweak-text rest nil lit)))
            ((or caps lit) (cons (char-upcase item) (tweak-text rest nil lit)))
            (t (cons (char-downcase item) (tweak-text rest nil nil)))))))

(defun game-print (lst)
  (princ (coerce (tweak-text (coerce (string-trim "() "
                                                  (prin1-to-string lst))
                                     'list)
                             t
                             nil)
                 'string))
  (fresh-line))
